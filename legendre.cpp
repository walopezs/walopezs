//modelo funcional que se acerca a predecir el número de números primos que hay hasta cierto número x
#include<iostream>
#include<cstdlib>
#include<math.h> //log
//using namespace std;
int main(int argc, char **argv)
{
  int const x=atoi(argv[1]);
  for(int i=0;i<=x;i++)
    {
      int numprimos=i/(log(i)-1.08366);
      std::cout<<i<<"\t"<<numprimos<<std::endl;
    }
}
